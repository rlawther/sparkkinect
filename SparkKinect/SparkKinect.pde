// Daniel Shiffman
// All features test

// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/

import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import codeanticode.syphon.*;
import java.util.Date;

Kinect kinect;
SyphonServer syphonServer = null;

static final int STRAIGHT_THROUGH = 0;
static final int BACKGROUND_SUBTRACT = 1;
static final int SUCCESSIVE_BACKGROUNDS = 2;
static final float BACKGROUND_THRESHOLD = 2;
static final int initialMaskFrames = 20;
int visionAlgorithm = 0;
int erodeSteps = 1;
float newBackgroundTimer = 3.0f;
float lastBackgroundTime;
float decay = 0;
int frameDelay = 0;

PImage depthImg;
PImage processedImg;
PImage prevProcessedImg;
PImage backgroundImg;
PImage backgroundMask;
PImage flippedImg;

float tiltDegrees;

boolean ir = false;
boolean colorDepth = false;
boolean mirror = false;

int frame = 0;
String[] filenames;
int fileCount = 0;
int maskFrameCount = 0;

// This function returns all the files in a directory as an array of Strings  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

void setup() {
  size(640, 640, P3D);
  
  kinect = new Kinect(this);
  if (kinect.numDevices() > 0)
  {
    kinect.initDepth();
    kinect.initVideo();
    kinect.enableColorDepth(colorDepth);
    tiltDegrees = kinect.getTilt();
    //kinect.tilt(deg);
  }
  else
  {
    kinect = null;
  }
  
  println("OS : " + System.getProperty("os.name"));
  if (System.getProperty("os.name").contains("Mac"))
  {
    syphonServer = new SyphonServer(this, "Processing Syphon");
  }
    
  String path = sketchPath();

  filenames = listFileNames(path + "/frames");

  processedImg = createImage(640, 480, RGB);
  backgroundMask = createImage(640, 480, RGB);
  flippedImg = createImage(640, 480, RGB);
}

void subtractBackground()
{
  int x, y; //<>//
  int pixelOffset = 0;
  if (backgroundImg == null)
    backgroundImg = depthImg.copy();
     
  for (y = 0; y < depthImg.height; y++)
  { //<>//
    for (x = 0; x < depthImg.width; x++)
    {
      //processedImg.pixels[pixelOffset] = color(255, 0, 0); //<>//
      //pixelOffset++;
      //continue;
      
      // a background mask with red set indicates a "noisy" area. A depth of 0 in a noisy area we count as noise so ignore it
      if ((red(backgroundMask.pixels[pixelOffset]) > 100) && (red(depthImg.pixels[pixelOffset]) == 0))
      {
        processedImg.pixels[pixelOffset] = color(0);
      }
      else
      {
        float diff = abs(red(depthImg.pixels[pixelOffset]) - red(backgroundImg.pixels[pixelOffset]));
        if (diff > BACKGROUND_THRESHOLD)
          processedImg.pixels[pixelOffset] = color(255);
        else
          processedImg.pixels[pixelOffset] = color(0);
      }
      if (pixelOffset < 10)
      {
        //println(red(processedImg.pixels[pixelOffset]));
      }
      pixelOffset++;
      
    }
  }
  for (x = 0; x < erodeSteps; x++)
    processedImg.filter(ERODE);
  for (x = 0; x < erodeSteps; x++)
    processedImg.filter(DILATE);
    
  processedImg.updatePixels();
}

void decayImg()
{
  int x, y;
  int pixelOffset = 0;
  int flippedOffset;
  float r, g, b;
  if (prevProcessedImg == null)
    prevProcessedImg = prevProcessedImg.copy();
     
  for (y = 0; y < processedImg.height; y++)
  {
    for (x = 0; x < processedImg.width; x++)
    {
      r = (red(prevProcessedImg.pixels[pixelOffset]) * decay) + 
        (red(processedImg.pixels[pixelOffset]) * (1 - decay));
      processedImg.pixels[pixelOffset] = color(r);
      pixelOffset++;
    }
  }  
  processedImg.updatePixels();
}

void flipImage()
{
  int x, y;
  int pixelOffset = 0;
  int flippedOffset;
  if (backgroundImg == null)
    backgroundImg = depthImg;
     
  for (y = 0; y < depthImg.height; y++)
  {
    for (x = 0; x < depthImg.width; x++)
    {
      flippedOffset = ((479 - y) * 640) + x;
      flippedImg.pixels[flippedOffset] = processedImg.pixels[pixelOffset];
      pixelOffset++;
    }
  }
  flippedImg.updatePixels();
}

void draw() {
  background(0);
  
  if (kinect != null)
  {
    depthImg = kinect.getDepthImage();
    //depthImg.save("frame" + str(millis()) + ".png");
  }
  else
  {
    fileCount++;
    if (fileCount >= filenames.length)
    {
      fileCount = 0;
      return;
    }
    depthImg = loadImage(sketchPath() + "/frames/" + filenames[fileCount], "png");
  }
  if (depthImg.width < 0)
    return;

  prevProcessedImg = processedImg.copy();    
  switch (visionAlgorithm)
  {
    case STRAIGHT_THROUGH:
      processedImg = depthImg.copy();
      break;
    case BACKGROUND_SUBTRACT:
      if (maskFrameCount > 0)
      {
        maskFrameCount--;
        addBackgroundMaskFrame();
      }
      subtractBackground();
      break;
    case SUCCESSIVE_BACKGROUNDS:
      if ((millis() - lastBackgroundTime) > (newBackgroundTimer * 1000))
      {
        backgroundImg = depthImg.copy();
        //backgroundMask = createImage(640, 480, RGB);
        maskFrameCount = initialMaskFrames;
        lastBackgroundTime = millis();
      }
      if (maskFrameCount > 0)
      {
        maskFrameCount--;
        addBackgroundMaskFrame();
      }
      subtractBackground();
      break;
  }
  decayImg();
  image(processedImg, 0, 0);
  //image(depthImg, 320, 0);
  frame++;
  if (syphonServer != null)
  {
    flipImage();
    syphonServer.sendImage(flippedImg);
  }
  fill(255);
  text("Mode " + visionAlgorithm + " : Press '0' for basic depth map, '1' for background subtraction, '2' for successive backgrounds", 10, 495);
  text("'e' to decrease, 'E' to increase Erode Steps : " + erodeSteps, 10, 515);
  text("'d' to decrease, 'D' to increase Decay : " + decay, 10, 535);
  text("'s' to decrease, 'S' to increase Successive Background Timing : " + newBackgroundTimer + " seconds", 10, 555);
  text("'f' to decrease, 'F' to increase Frame Delay : " + frameDelay + " milliseconds", 10, 575);
  text("'t' to decrease, 'T' to increase Tilt : " + tiltDegrees, 10, 595);
  if (frameDelay > 0)
    delay(frameDelay);
}


void addBackgroundMaskFrame()
{
  int x, y;
  int pixelOffset = 0;    
  
  for (y = 0; y < depthImg.height; y++)
  {
    for (x = 0; x < depthImg.width; x++)
    {
      // if the depth == 0, mark pixel as "noisy"
      if (red(depthImg.pixels[pixelOffset]) == 0)
        backgroundMask.pixels[pixelOffset] = color(255, 0, 0);
      // if we've never seen a valid value here, we record that into the background
      else if (red(backgroundImg.pixels[pixelOffset]) == 0)
        backgroundImg.pixels[pixelOffset] = depthImg.pixels[pixelOffset];
      pixelOffset++;
    }
  }
}
void keyPressed() {
  if (key == '0')
    visionAlgorithm = 0;
  if (key == '1')
  {
    visionAlgorithm = 1;
    backgroundImg = depthImg.copy();
    backgroundMask = createImage(640, 480, RGB);
    maskFrameCount = initialMaskFrames;
  }
  if (key == '2')
  {
    visionAlgorithm = 2;
    backgroundImg = depthImg.copy();
    backgroundMask = createImage(640, 480, RGB);
    maskFrameCount = initialMaskFrames;
    lastBackgroundTime = millis();
  }
  if (key == 'b')
  {
    println("Setting background");
    backgroundImg = depthImg.copy();
    backgroundMask = createImage(640, 480, RGB);
  }
  if (key == 'm')
    addBackgroundMaskFrame();
  if (key == 'e')
    erodeSteps--;
  if (key == 'E')
    erodeSteps++;

  if (key == 's')
  {
    newBackgroundTimer -= 0.1;
    if (newBackgroundTimer < 0)
      newBackgroundTimer = 0;
  }
  if (key == 'S')
  {
    newBackgroundTimer += 0.1;
  }
  if (key == 'd')
  {
    decay -= 0.05;
    if (decay < 0)
      decay = 0;
  }
  if (key == 'D')
  {
    decay += 0.05;
    if (decay > 1)
      decay = 1;
  }
  if (key == 'f')
  {
    frameDelay -= 10;
    if (frameDelay < 0)
      frameDelay = 0;
  }
  if (key == 'F')
  {
    frameDelay += 10;
  }
  if (key == 't')
  {
    tiltDegrees -= 1;
    if (tiltDegrees < 0)
      tiltDegrees = 0;
    kinect.setTilt(tiltDegrees);
  }
  if (key == 'T')
  {
    tiltDegrees += 1;
    if (tiltDegrees > 30)
      tiltDegrees = 30;
    kinect.setTilt(tiltDegrees);
  }
    
  return;
  /*
  if (key == 'i') {
    ir = !ir;
    kinect.enableIR(ir);
  } else if (key == 'c') {
    colorDepth = !colorDepth;
    kinect.enableColorDepth(colorDepth);
  }else if(key == 'm'){
    mirror = !mirror;
    kinect.enableMirror(mirror);
  } else if (key == CODED) {
    if (keyCode == UP) {
      deg++;
    } else if (keyCode == DOWN) {
      deg--;
    }
    deg = constrain(deg, 0, 30);
    kinect.setTilt(deg);
  }
  */
}
